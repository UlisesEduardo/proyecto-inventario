﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Inventario.Models;

namespace Inventario.Controllers
{
    public class SucursalesController : Controller
    {
        private InventarioEntities db = new InventarioEntities();

        // GET: Sucursales
        public ActionResult Index()
        {
            return View(db.sucursales.ToList());
        }

        // GET: Sucursales/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sucursales sucursales = db.sucursales.Find(id);
            if (sucursales == null)
            {
                return HttpNotFound();
            }
            //accedemos a los productos disponibles en esa sucursal, lo pasamos a la vista
            //con el ViewBag para rellenar la tabla
            ViewBag.productos = db.SP_viewProducts(id).ToList();
            return View(sucursales);
        }

        // GET: Sucursales/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sucursales/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id_sucursal,nombre")] sucursales sucursales)
        {
            if (ModelState.IsValid)
            {
                db.sucursales.Add(sucursales);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sucursales);
        }

        // GET: Sucursales/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sucursales sucursales = db.sucursales.Find(id);
            if (sucursales == null)
            {
                return HttpNotFound();
            }
            return View(sucursales);
        }

        // POST: Sucursales/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id_sucursal,nombre")] sucursales sucursales)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sucursales).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sucursales);
        }

        // GET: Sucursales/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            sucursales sucursales = db.sucursales.Find(id);
            if (sucursales == null)
            {
                return HttpNotFound();
            }
            return View(sucursales);
        }

        // POST: Sucursales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            sucursales sucursales = db.sucursales.Find(id);
            db.sucursales.Remove(sucursales);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // Permite actualizar el stock al agregar producto, esto solo se hace la sucursal
        // donde se registra el movimiento
        public ActionResult AddStock(int id_producto, int id_sucursal, int cantidad)
        {
            db.SP_addStock(id_producto, id_sucursal, cantidad);
            return RedirectToAction("Index");
        }

        // Permite actualizar el stock al vender un producto, esto solo se hace la sucursal
        // donde se registra el movimiento
        public ActionResult DiscountStock(int id_pro, int id_suc, int cantidad)
        {
            db.SP_discountStock(id_pro, id_suc, cantidad);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
